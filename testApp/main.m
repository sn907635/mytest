//
//  main.m
//  testApp
//
//  Created by Nick.H on 12/9/21.
//  Copyright (c) 2012年 Nick.H. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "appsgooAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([appsgooAppDelegate class]));
    }
}
