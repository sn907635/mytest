//
//  appsgooViewController.m
//  testApp
//
//  Created by Nick.H on 12/9/21.
//  Copyright (c) 2012年 Nick.H. All rights reserved.
//

#import "appsgooViewController.h"

@interface appsgooViewController ()

@end

@implementation appsgooViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

@end
